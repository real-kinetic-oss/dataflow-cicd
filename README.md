# dataflow-cicd
This repository is used for learning purposes. \
It houses the code needed to build and deploy a Dataflow flex-template written in Python.

Components:
- .gitlab-ci.yml: CI/CD pipeline template code.
- Dockerfile: Compiles the Apache Beam code, creates image to be run in the pipeline.
- metadata.json: Dataflow pipeline metadata spec.
- count_produce.py: Source code written in Python for Apache Beam pipeline

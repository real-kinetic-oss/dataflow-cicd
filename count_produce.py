import argparse
import logging

import apache_beam as beam
from apache_beam.io.textio import WriteToText
from apache_beam.options.pipeline_options import PipelineOptions


def run(argv=None) -> None:
    parser = argparse.ArgumentParser(
        prog="Count Produce",
        description="Count produce by type using Apache Beam.",
        epilog="If you want more help reach out to the guys at Real Kinetic! https://realkinetic.com/#contact",
    )
    parser.add_argument(
        "--output",
        required=True,
        help="The path prefix for output file.",
    )
    known_args, pipeline_args = parser.parse_known_args(argv)

    pipeline_options = PipelineOptions(pipeline_args)

    with beam.Pipeline(options=pipeline_options) as pipeline:
        (
            pipeline
            | "Create produce"
            >> beam.Create(["🍓", "🥕", "🥕", "🥕", "🍆", "🍆", "🍅", "🍅", "🍅", "🌽"])
            | "Count all unique elements" >> beam.combiners.Count.PerElement()
            | "Write to GCS" >> WriteToText(known_args.output, file_name_suffix=".txt")
        )


if __name__ == "__main__":
    logging.getLogger().setLevel(logging.INFO)
    run()
